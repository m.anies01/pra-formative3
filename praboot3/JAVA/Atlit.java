public class Atlit implements Biodata, Club {
    String nama = "Ibrahimovic",
            negara = "Sweden",
            bidang = "Sepakbola",
            club  ="AC Milan";
    int skill = 80;

    public void printNama() {
        System.out.println("Nama Atlit: "+nama);
    }

    public void printNegara() {
        System.out.println("Negara: "+negara);
    }

    public void printBidang() {
        System.out.println("Bidang: "+bidang);
    }

    public void printSkill() {
        System.out.println("Rate Skill 1 - 100 : "+skill);
    }
    public void printAsalClub(){
        System.out.println("Nama Club: "+club);
    }
}
